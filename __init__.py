# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "EWI Remote Exporter",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 6),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}


import bpy
from bpy.props import StringProperty
from . import render
import os


def show_message(message = "", title = "Message Box", icon = 'INFO'):
    """Displays small message box to user.
    
    - message : message to be displayed
    - title : title of the message box
    - icon : icon to be display ('INFO', 'WARNING', 'ERROR')"""
    
    def draw(self, context):
        lines = message.split("\n")
        for line in lines :
            self.layout.label(text=line)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)


class EWI_OT_QUICKEXPORT(bpy.types.Operator):
    bl_idname = "ewi.quickexport"
    bl_label = " Remote exports"
    bl_options = {'UNDO'}

    def execute(self, context) :
        ffmpeg_path = prefs().ffmpeg_path
        if ffmpeg_path == "" :
            bpy.ops.ewi.find_ffmpeg()
            if prefs().ffmpeg_path == "" :
                show_message("Please set-up a valid FFMPEG location in the addon preferences.", "ERROR")
                return {'CANCELLED'}
            else : ffmpeg_path = prefs().ffmpeg_path
        
        error_msg = render.export_scenes(ffmpeg_path)
        
        if error_msg is not None :
            show_message(error_msg, "ERROR")
            return {'CANCELLED'}
        
        return {'FINISHED'}

class EWI_OT_FIND_FFMPEG(bpy.types.Operator):
    bl_idname = "ewi.find_ffmpeg"
    bl_label = " Find FFMPEG"
    bl_options = {'UNDO'}

    def execute(self, context) :
        addon_prefs = context.preferences.addons[__package__].preferences
        possible_paths = ["C:\\tools\\ffmpeg-7.0\\bin\\ffmpeg.exe",
                          "C:\\FFmpeg\\bin\\ffmpeg.exe"
                          ]
        for path in possible_paths :
            if os.path.exists(path) : 
                addon_prefs.ffmpeg_path = path
                return {'FINISHED'}

        self.report({'ERROR'}, message = "Unable to guess ffmpeg path, Please contact your TD")
        return {'CANCELLED'}

         
        
        
  
class QUICKEXPORT_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Remote exporting"
    bl_idname = "QUICKEXPORT_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):
        
        layout = self.layout
        addon_prefs = context.preferences.addons[__package__].preferences
        row = layout.row()
        
        if not os.path.exists(addon_prefs.ffmpeg_path):
            row.label(text= 'WARNING, your FFMPEG path is incorrect', icon = 'ERROR')
            row = layout.row()
            row.prop(addon_prefs, "ffmpeg_path", text = '', icon = 'ERROR') 
            row.operator("ewi.find_ffmpeg")
        row = layout.row()
        row.operator('ewi.quickexport')

class EWI_Exporter_AddonPref(bpy.types.AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    
    ffmpeg_path : StringProperty(name="FFMPEG Path", default="", subtype='FILE_PATH')

    def draw(self, context):
        layout = self.layout
        row = layout.row()

        row.prop(self, "ffmpeg_path")


def prefs() :
    module_name = __name__
    return bpy.context.preferences.addons[module_name].preferences

        



classes = [ 
    EWI_OT_QUICKEXPORT,
    QUICKEXPORT_PT_PANEL,
    EWI_Exporter_AddonPref,
    EWI_OT_FIND_FFMPEG
]

def register():
    for cls in classes :
        bpy.utils.register_class(cls)  


def unregister():
    for cls in classes :
        bpy.utils.unregister_class(cls)
