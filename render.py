
import bpy
from bpy.props import StringProperty
import os
import uuid
import tempfile
import subprocess
import shutil
import platform


pop = lambda l : l[0] if len(l) > 0 else None


def get_render_scenes() :
    scenes = [s for s in bpy.data.scenes if not s.library]
    if len(scenes) == 1 :
        return [scenes[0]]
    return [s for s in scenes if s.name != "SCN_BUILD" and s.name.startswith("SCN_")]
    

def get_camera(scene) :
    for col in scene.collection.children_recursive :
        if "COL-RDR_CAM" in col.name :
            for o in col.all_objects :
                if type(o.data).__name__ == "Camera" :
                    return o
                
def ffmpeg_conv(png_path, mp4_path, ffmpeg_path, shot_name, delete=True) :

    osfonts = {
        "Windows" : r"C\\:/Windows/fonts/consola.ttf",
        "Darwin" : "/Library/Fonts/Menlo.ttc", # needs to be tested on mac
    }

    font_path = osfonts[platform.system()]

    prompt = [
        f'"{ffmpeg_path}"',
        '-y',
        f'-i "{png_path}"',
        '-vf ',
        '"drawtext=text=\'%%{eif\:n+1\:d\:4}\':x=w-50-tw:y=50:fontfile=%s:fontcolor=yellow:fontsize=32:borderw=1:bordercolor=black:alpha=0.4,' % font_path,
        'drawtext=text=\'%s\':x=50:y=50:fontfile=%s:fontcolor=yellow:fontsize=32:borderw=1:bordercolor=black:alpha=0.4"' % (shot_name, font_path),
        '-pix_fmt yuvj420p',
        '-crf 18',
        f'"{mp4_path}"'
    ]

    cmd = " ".join(prompt)
    print(cmd)
    p = subprocess.Popen(cmd)
    p.wait()

    if delete :
        dirpath = os.path.dirname(png_path)
        print("Deleting : ", dirpath)
        shutil.rmtree(dirpath)

        
def shot_full_name(main_name, scene_name) :
    get_tag = lambda tag, name : pop([e for e in name.split("_") if e.startswith(tag)])
    replace_tag = lambda name, tag, new_value : "_".join([new_value if e.startswith(tag) else e for e in name.split("_")])
    
    new_name = main_name

    if 'SQ' in scene_name and 'SH' in scene_name :
        new_name = replace_tag(new_name, "SQ", get_tag('SQ', scene_name))
        new_name = replace_tag(new_name, "SH", get_tag('SH', scene_name))
    
    return new_name


def export_scenes(ffmpeg_path) :

    output_path = os.path.dirname(bpy.data.filepath)
    main_name = os.path.basename(bpy.data.filepath).replace(".blend", "")
    tmp_dir = tempfile.mkdtemp(prefix=main_name)

    print("TMP : ", tmp_dir)
    print("FFMPEG pref : ", ffmpeg_path)

    for scene in get_render_scenes() :
        print('RENDERING ' + scene.name)
        bpy.context.window.scene = scene #make the scene to render active

        cam = get_camera(scene)
        if cam is None : 
            return f"No camera was found for {scene.name}"
                
        file_name = f"{main_name}_{scene.name}.mp4"

        scene.camera = cam
        scene.render.filepath = os.path.join(tmp_dir, file_name)
        scene.render.image_settings.file_format = 'FFMPEG'
        scene.render.ffmpeg.format = 'MPEG4'
        scene.render.ffmpeg.codec = 'H264'
        scene.render.ffmpeg.audio_codec = 'AAC'
        scene.render.use_file_extension = True 
        scene.render.ffmpeg.ffmpeg_preset = 'BEST'
        scene.render.ffmpeg.constant_rate_factor = "LOSSLESS"

        try :
            bpy.ops.render.render(animation=True)      
        
        except Exception as e :
            return f"{scene.name} was unable to render."

        # print("Rendered to : ", scene.render.filepath)
        output_name = shot_full_name(main_name, scene.name)
        ffmpeg_conv(scene.render.filepath, os.path.join(output_path, output_name+".mp4"), ffmpeg_path, output_name, delete=True)

if __name__ == "__main__" :
    print("EWI_Exporter is being executed as a standalone.")
    # TO BE CHANGED ACCORDING TO YOUR ENVIRONMENT
    ffmpeg_path = r"C:\tools\ffmpeg-7.0\bin\ffmpeg.exe" 
    export_scenes(ffmpeg_path)
